#!/bin/bash

helpers/cpuperformance.sh
helpers/killapps.sh

# Start mongo.
pushd ~/src/third_party/mongo-2.5.4
scons -j8 --extralib=lttng-ust,dl
rm -rf ~/Desktop/db
mkdir ~/Desktop/db
LD_PRELOAD=/usr/local/lib/liblttng-profile.so ./mongod --dbpath ~/Desktop/db&
sleep 30
popd

# Start tracing.
lttng create usecase-mongo-groupcommitmutex
helpers/events.sh
lttng start

# Start test application.
LD_PRELOAD=/usr/local/lib/liblttng-profile.so ../tk-mongowrite

# Stop tracing.
lttng destroy

# Kill mongo.
sudo killall mongod
