#!/bin/bash

helpers/cpuperformance.sh
helpers/killapps.sh

mkdir /tmp/trace-kit-io-files/

# Start tracing.
lttng create usecase-fsync
helpers/events.sh
lttng start

# Start test application.
LD_PRELOAD=/usr/local/lib/liblttng-profile.so ../tk-fsync

# Stop tracing.
lttng destroy
