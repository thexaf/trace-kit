#!/usr/bin/env python3
#
# The MIT License (MIT)
#
# Copyright (C) 2015 - Francois Doray <francois.pierre-doray@polymtl.ca>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import csv
import math
import statistics

def Percentile(column, percent, high=True):
    k = (len(column)-1) * percent
    if high:
        return column[math.ceil(k)]
    else:
        return column[math.floor(k)]

def PrintStats(filename, field):
    # Read data.
    column = []
    with open(filename, 'r') as filecontent:
        index = -1
        reader = csv.reader(filecontent)
        for row in reader:
            # Get field index.
            if index == -1:
                if field is None:
                    index = 0
                else:
                    for curindex, curfield in enumerate(row):
                        if curfield == field:
                            index = curindex
                            break
                if index == -1:
                    print('Invalid field name.')
                    return
                continue
            column.append(eval(row[index]))

    print('Mean: {0}'.format(statistics.mean(column)))
    print('Standard deviation: {0}'.format(statistics.stdev(column)))
    print('95% of values between {0} and {1}'.format(
        Percentile(column, 0.025),
        Percentile(column, 0.975)))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Stats from CSV')
    parser.add_argument('filename', metavar="<path/to/csv>", help='CSV file name')
    parser.add_argument('--field', default=None, help='CSV field name')
    args = parser.parse_args()
    PrintStats(args.filename, args.field)
