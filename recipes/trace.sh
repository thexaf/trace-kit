#!/bin/bash

sudo control-addons.sh load

lttng create trace
helpers/events.sh
lttng start

echo 'Press any key to stop tracing.'
read

lttng destroy
echo 'Stopped tracing.'
