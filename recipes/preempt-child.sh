#!/bin/bash

# Move this task to tkpreemptset cpu set.
sudo cset proc --move $$ tkpreemptset

# Start test application.
LD_PRELOAD=/usr/local/lib/liblttng-profile.so ../tk-preempt
