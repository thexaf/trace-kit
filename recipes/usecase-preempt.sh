#!/bin/bash

#sudo control-addons.sh load

sudo cset set --destroy tkpreemptset > /dev/null
sudo cset set --cpu=2 --set=tkpreemptset

lttng create usecase-preempt
helpers/all-events.sh
lttng start

# Run the application.
sudo ./preempt-child.sh

lttng destroy

sudo cset set --destroy tkpreemptset
