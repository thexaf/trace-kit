#!/bin/bash

helpers/cpuperformance.sh
helpers/killapps.sh

# Start mongo.
pushd ~/src/third_party/mongo-3.0.0rc10
scons -j8 --extralib=lttng-ust,dl
rm -rf ~/Desktop/db
mkdir ~/Desktop/db
LD_PRELOAD=/usr/local/lib/liblttng-profile.so ./mongod --dbpath ~/Desktop/db&
sleep 30
popd

# Start tracing.
lttng create usecase-mongo-sleep
helpers/events.sh
lttng start

# Start test application.
~/src/third_party/mongo-3.0.0rc10/mongo ~/src/third_party/mongo-3.0.0rc10/batch_insert.js

# Stop tracing.
lttng destroy

# Kill mongo.
sudo killall mongod
