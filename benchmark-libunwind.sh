#!/bin/bash

NAME="benchmarks/benchmark_unw_backtrace_"

for ((i = 0; i <= 20; i += 2))
do
   ./tk-libunwind $i > $NAME$i.csv
done
