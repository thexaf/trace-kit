// Copyright (c) 2015 Francois Doray <francois.pierre-doray@polymtl.ca>
//
// This file is part of trace-kit.
//
// trace-kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// trace-kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with trace-kit.  If not, see <http://www.gnu.org/licenses/>.
#include <chrono>
#include <iostream>
#include <thread>

#include "utils/attributes.h"
#include "utils/priority.h"
#include "utils/scoped_tp.h"
#include "utils/use_cpu.h"

const size_t kNumRepetitions = 400000;
const size_t kAnnoyingThreadSleepTime = 20;

volatile bool done = false;

void NOINLINE DoSomethingImportant()
{
    ScopedTp tp;
    UseCpu(10000000);
}

void NOINLINE ImportantThread()
{
    SetPriority(1);
    for (size_t i = 0; i < kNumRepetitions; ++i) {
        DoSomethingImportant();
        if (i % 1000 == 0)
            std::cout << i << " / " << kNumRepetitions << std::endl;
    }
    done = true;
}

void NOINLINE DoSomethingAnnoying()
{
    UseCpu(1000000);
}

void NOINLINE AnnoyingThread()
{
    SetPriority(20);
    while (!done)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(
            kAnnoyingThreadSleepTime));
        DoSomethingAnnoying();
    }
}

int main()
{
    std::thread importantThread(ImportantThread);
    std::thread annoyingThread(AnnoyingThread);
    importantThread.join();
    annoyingThread.join();
    return 0;
}
