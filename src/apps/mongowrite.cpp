// Copyright (c) 2015 Francois Doray <francois.pierre-doray@polymtl.ca>
//
// This file is part of trace-kit.
//
// trace-kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// trace-kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with trace-kit.  If not, see <http://www.gnu.org/licenses/>.
#include <boost/random/random_device.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <iostream>
#include <mongo/bson/bson.h>
#include <mongo/client/dbclient.h>
#include <mongo/client/dbclientinterface.h>
#include <unordered_set>
#include <vector>

#include "utils/attributes.h"
#include "utils/scoped_tp.h"

const char kMongoHost[] = "localhost:27017";
const char kMongoCollection[] = "tracekit.mongowrite";

class RandomStringGenerator
{
public:
    RandomStringGenerator()
        : chars("abcdefghijklmnopqrstuvwxyz"
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"),
          index_dist(0, chars.size() - 1)
    {
    }

    std::string GenerateRandomString(size_t length)
    {
        std::string str;
        for (size_t i = 0; i < length; ++i)
            str += chars[index_dist(rng)];
        return str;
    }

private:
    std::string chars;
    boost::random::random_device rng;
    boost::random::uniform_int_distribution<> index_dist;
};

void NOINLINE GenerateSomeData(RandomStringGenerator& generator,
                               mongo::BSONObjBuilder& bson)
{
    std::unordered_set<std::string> keys;
    for (size_t i = 0; i < 10; ++i)
    {
        auto key = generator.GenerateRandomString(15);
        while (keys.find(key) != keys.end())
            key = generator.GenerateRandomString(15);
        keys.insert(key);

        auto value = generator.GenerateRandomString(1000);
        bson.append(key, value);
    }
}

void NOINLINE InsertInDatabase(mongo::BSONObjBuilder& bson,
                               mongo::DBClientConnection& connection)
{
    connection.insert(kMongoCollection, bson.obj());
}

void NOINLINE DoSomeActions(RandomStringGenerator& generator,
                            mongo::DBClientConnection& connection)
{
    ScopedTp scoped_tp;
    mongo::BSONObjBuilder bson;
    GenerateSomeData(generator, bson);
    InsertInDatabase(bson, connection);
}

int main()
{
    mongo::client::initialize();

    RandomStringGenerator generator;
    mongo::DBClientConnection connection(true);
    std::string errMsg;
    if (!connection.connect(kMongoHost, errMsg))
    {
        std::cerr << "Error while connecting to mongodb: " << errMsg
                  << std::endl;
        return 1;
    }

    for (size_t i = 0; i < 10000; ++i)
    {
        DoSomeActions(generator, connection);

        if (i % 100 == 0)
            std::cout << i << " / 10000 =========================" << std::endl;
    }

    return 0;
}
