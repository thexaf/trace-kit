// Copyright (c) 2014 Francois Doray <francois.pierre-doray@polymtl.ca>
//
// This file is part of trace-kit.
//
// trace-kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// trace-kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with trace-kit.  If not, see <http://www.gnu.org/licenses/>.

#include <signal.h>

#include "utils/attributes.h"
#include "utils/scoped_tp.h"
#include "utils/use_cpu.h"

void NOINLINE DoUselessStuff(long i)
{
    UseCpu(10000000 * ((i / 10) + 1));
}

void NOINLINE MyChildFunction(int i)
{
    DoUselessStuff(i);
}

void NOINLINE MyFunctionA(int i)
{
    DoUselessStuff(5);
    MyChildFunction(i);
}

void NOINLINE MyFunctionB(int i)
{
    DoUselessStuff(5);
    MyChildFunction(i);
}

int main()
{
    for (int i = 0; i < 100; ++i)
    {
        MyFunctionA(i);
        MyFunctionB(i);
    }
}
