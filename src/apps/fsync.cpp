// Copyright (c) 2015 Francois Doray <francois.pierre-doray@polymtl.ca>
//
// This file is part of trace-kit.
//
// trace-kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// trace-kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with trace-kit.  If not, see <http://www.gnu.org/licenses/>.
#include <fcntl.h>
#include <iostream>
#include <stdlib.h>
#include <thread>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>

#include "utils/scoped_tp.h"
#include "utils/time.h"
#include "utils/tp.h"

bool done = false;

void FsyncFunction()
{
    const size_t kBufferSize = 4096;
    int fd = open("/tmp/trace-kit-io-files/log.bin", O_WRONLY | O_CREAT | O_TRUNC,
        S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    if (fd == -1) {
        std::cout << "unable to open output file... " << strerror(errno) << std::endl;
        return;
    }

    for (size_t i = 0; i < 10; ++i)
    {
        uint64_t buffer[kBufferSize];
        for (size_t j = 0; j < kBufferSize; ++j)
            buffer[j] = rand();
        write(fd, buffer, kBufferSize);
        fsync(fd);
    }
    close(fd);
}

void FsyncThread()
{
    while (!done)
    {
        sleep(1);
        FsyncFunction();
    }
}

void ReadFunction(size_t offset)
{
    ScopedTp tp;

    int fd = open("hugefile.bin", O_RDONLY);
    lseek(fd, rand() % 2000000000, SEEK_SET);

    uint64_t total = 0;
    uint64_t buffer = 0;

    size_t count = 0;
    while (read(fd, &buffer, sizeof(buffer)) > 0) {
        total += buffer;
        ++count;
        if (count > 10)
            break;
    }

    close(fd);
    if (total == 0)
        std::cout << total << std::endl;
}

void ReadThread()
{
    for (size_t i = 0; i < 20000; ++i)
    {
        if (i % 1000 == 0)
            std::cout << i << " / " << 20000 << std::endl;
        ReadFunction(i * 10);
    }
    done = true;
}

int main()
{
    srand(time(NULL));

    std::thread fsyncThread(FsyncThread);
    std::thread readThread(ReadThread);

    fsyncThread.join();
    readThread.join();
}
