// Copyright (c) 2015 Francois Doray <francois.pierre-doray@polymtl.ca>
//
// This file is part of trace-kit.
//
// trace-kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// trace-kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with trace-kit.  If not, see <http://www.gnu.org/licenses/>.

#include <fstream>
#include <signal.h>

#include "utils/attributes.h"
#include "utils/scoped_tp.h"
#include "utils/time.h"

const char kDummyData[] = {
    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
};

void NOINLINE WriteSomeData(std::fstream& out)
{
    out.write(kDummyData, sizeof(kDummyData));
}

void NOINLINE MyFunctionB(size_t index)
{
    std::fstream out("/tmp/trace-kit-io-files/" + std::to_string(index) + ".bin",
                     std::ios::out | std::ios::binary);
    for (size_t i = 0; i < 100; ++i)
        WriteSomeData(out);
}

void NOINLINE MyFunctionA(size_t i)
{
    MyFunctionB(i);
}

void NOINLINE DoIt(size_t i)
{
    ScopedTp tp;
    MyFunctionA(i);
}

int main()
{
    for (size_t i = 0; i < 5000000; ++i)
    {
        DoIt(i);
    }
}
