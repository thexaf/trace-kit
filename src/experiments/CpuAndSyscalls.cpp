// Copyright (c) 2015 Francois Doray <francois.pierre-doray@polymtl.ca>
//
// This file is part of trace-kit.
//
// trace-kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// trace-kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with trace-kit.  If not, see <http://www.gnu.org/licenses/>.
#include <iostream>
#include <math.h>
#include <unistd.h>

#include "utils/attributes.h"
#include "utils/time.h"

namespace
{

const int kMaxPrime = 25000;
const int kNumIterations = 5000;
const int kNumTests = 10;

int maxDepth = 0;

int NOINLINE TestPrime()
{
  // This code comes from sysbench.
  // https://github.com/akopytov/sysbench/blob/0.5/sysbench/tests/cpu/sb_cpu.c
  // GNU GPL version 2.
  int n = 0;
  for(int c = 3; c < kMaxPrime; c++)  
  {
    int t = sqrt((double)c);
    int l = 2;
    for(; l <= t; l++)
      if (c % l == 0)
        break;
    if (l > t )
      n++; 
  }
  return n;
}

int NOINLINE DoTest()
{
    int total = 0;
    for (size_t i = 0; i < kNumIterations; ++i)
    {
        total += TestPrime();
        usleep(1000);
    }
    return total;
}

int NOINLINE Recursive(int depth)
{
    if (depth == maxDepth)
        return DoTest();
    return Recursive(depth + 1);
}

}  // namespace

int main(int argc, const char* argv[])
{
    maxDepth = atoi(argv[1]);

    std::cout << "Duration " << maxDepth << std::endl;
    for (size_t i = 0; i < kNumTests; ++i)
    {
        auto begin = GetMonotonicTime();
        int total = Recursive(0);
        auto end = GetMonotonicTime();
        std::cout << (end - begin) << std::endl;

        // Use |total|.
        if (total == -1)
            std::cout << total << std::endl;
    }
}
