// Copyright (c) 2015 Francois Doray <francois.pierre-doray@polymtl.ca>
//
// This file is part of trace-kit.
//
// trace-kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// trace-kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with trace-kit.  If not, see <http://www.gnu.org/licenses/>.
#include <assert.h>
#include <iostream>
#include <signal.h>
#include <string>
#include <string.h>
#include <unistd.h>

#include "utils/scoped_tp.h"
#include "utils/tp.h"
#include "utils/use_cpu.h"

const char kMessage = 'm';

int numSignalsReceived = 0;

void Handler(int signo, siginfo_t* info, void* arg)
{
    ScopedTp tp;
    tracepoint(tracekit, value, signo);
    raise(SIGUSR1);
    UseCpu(100000);
}

int Receiver()
{
    // Setup signal handler.
    struct sigaction sigact;
    memset(&sigact, 0, sizeof(sigact));
    sigact.sa_sigaction = &Handler;
    sigact.sa_flags = SA_SIGINFO;
    if (sigaction(SIGUSR1, &sigact, NULL) != 0)
    {
        std::cerr << "Sigaction error." << std::endl;
        return 1;
    }

    if (sigaction(SIGUSR2, &sigact, NULL) != 0)
    {
        std::cerr << "Sigaction error." << std::endl;
        return 1;
    }

    return 0;
}

int Sender()
{
    // Setup communication pipe.
    int communication_pipe[2];
    pipe(communication_pipe);

    // Create child receiver process.
    pid_t child = fork();
    if (child < 0)
    {
        std::cerr << "Fork error." << std::endl;
        return 1;
    }

    if (child == 0)
    {
        // We are in the receiver process.

        // Close input pipe.
        close(communication_pipe[0]);

        // Setup signal handler.
        if (Receiver() != 0)
            return 1;

        // Inform the parent process that the signal handler is ready.
        std::cout << "[receiver] Ready to receive signals." << std::endl;
        write(communication_pipe[1], &kMessage, sizeof(kMessage));

        // Wait forever.
        while (true)
            sleep(5);
    }

    // Close output pipe.
    close(communication_pipe[1]);

    // Wait until the child process has prepared its signal handler.
    char buffer[sizeof(kMessage)];
    read(communication_pipe[0], &buffer, sizeof(buffer));

    // We are in the sender process.
    // Continuously send SIGUSR1 to the receiver process.
    sleep(1);

    std::cout << "[sender] Starting to send signals." << std::endl;
    for (size_t i = 0; i < 10000; ++i) {
        kill(child, SIGUSR1);
        kill(child, SIGUSR2);
    }

    sleep(1);

    // Kill child process.
    kill(child, SIGKILL);

    return 0;
}

int main(int argc, const char* argv[])
{
    Sender();
}
