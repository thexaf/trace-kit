// Copyright (c) 2015 Francois Doray <francois.pierre-doray@polymtl.ca>
//
// This file is part of trace-kit.
//
// trace-kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// trace-kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with trace-kit.  If not, see <http://www.gnu.org/licenses/>.
#include "LibunwindBacktrace.h"

#define UNW_LOCAL_ONLY
#include <libunwind.h>

#define USE_BACKTRACE

size_t Backtrace(void** stack, size_t size)
{
#ifdef USE_BACKTRACE
        return unw_backtrace (stack, size);
#else
        unw_context_t unwind_context;
        unw_getcontext(&unwind_context);

        unw_cursor_t cursor;
        int ret = unw_init_local(&cursor, &unwind_context);
        if (ret != 0)
            return 0;

        size_t i = 0;
        for (; i < size; ++i)
        {
            void* ip = NULL;
            if (unw_get_reg(&cursor, UNW_REG_IP, (unw_word_t*)&ip) < 0)
                break;

            stack[i] = ip;
            if (unw_step(&cursor) <= 0)
                break;
        }

        return i;
#endif
}
