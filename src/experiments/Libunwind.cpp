// Copyright (c) 2015 Francois Doray <francois.pierre-doray@polymtl.ca>
//
// This file is part of trace-kit.
//
// trace-kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// trace-kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with trace-kit.  If not, see <http://www.gnu.org/licenses/>.
#include <iostream>
#include <stddef.h>
#include <stdio.h>

#define UNW_LOCAL_ONLY
#include <libunwind.h>

#include "utils/attributes.h"

namespace
{

const size_t kStackSize = 50;
const size_t kRecursionDepth = 5;
const size_t kIterations = 10000000;

// Backtrace.
size_t UnwBacktrace(void** stack, size_t size)
{
  return unw_backtrace (stack, size);
}

void NOINLINE RecursiveUnwBacktrace(int i)
{
	if (i == kRecursionDepth)
	{
		void* stack[kStackSize];
		UnwBacktrace(stack, kStackSize);
		return;
	}
	RecursiveUnwBacktrace(i + 1);
}

// Step.
size_t UnwStep(void** stack, size_t size)
{
  return unw_backtrace (stack, size);
}

void NOINLINE RecursiveStep(int i)
{
	if (i == kRecursionDepth)
	{
		void* stack[kStackSize];
		UnwStep(stack, kStackSize);
		return;
	}
	RecursiveStep(i + 1);
}

}  // namespace

int main(int argc, const char* argv[])
{
	if (argc != 2)
	{
		std::cerr << "Invalid number of arguments." << std::endl;
		return 1;
	}

	if (std::string(argv[1]) == "backtrace")
	{
		RecursiveUnwBacktrace(0);
	}
	else
	{
		RecursiveStep(0);
	}
}
