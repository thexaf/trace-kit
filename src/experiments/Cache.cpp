// Copyright (c) 2015 Francois Doray <francois.pierre-doray@polymtl.ca>
//
// This file is part of trace-kit.
//
// trace-kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// trace-kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with trace-kit.  If not, see <http://www.gnu.org/licenses/>.
#include <iostream>
#include <stddef.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "utils/attributes.h"
#include "utils/scoped_tp.h"
#include "utils/use_cpu.h"

void NOINLINE FunctionA()
{
    for (size_t i = 0; i < 25; ++i)
    {
        size_t duration = 100000 + (rand() % 100);
        UseCpu(duration);
    }
}

void NOINLINE FunctionC()
{
    for (size_t i = 0; i < 25; ++i)
    {
        size_t duration = 100000 + (rand() % 100);
        UseCpu(duration);
    }
}

void NOINLINE FunctionB()
{
    for (size_t i = 0; i < 25; ++i)
    {
        size_t duration = 1000 + (rand() % 10);
        UseCpu(duration);
    }
    FunctionC();
}

void NOINLINE SomethingComplexE()
{
    for (size_t i = 0; i < 25; ++i)
    {
        size_t duration = 1000000 + (rand() % 10000);
        UseCpu(duration);
    }
}

void NOINLINE SomethingComplexD()
{
    SomethingComplexE();
}

void NOINLINE SomethingComplexC()
{
    SomethingComplexD();
}

void NOINLINE SomethingComplexB()
{
    SomethingComplexC();
}

void NOINLINE SomethingComplexA()
{
    SomethingComplexB();
}

void NOINLINE HandleRequest(int i)
{
    ScopedTp scoped_tp;
    FunctionA();
    FunctionB();
    if (i % 4 == 0)
        SomethingComplexA();
}

int main()
{
    srand(time(NULL));

    for (size_t i = 0; i < 1000; ++i)
    {
        //std::cout << "request " << i << std::endl;
        HandleRequest(i);
        usleep(50000);

        if (i % 100 ==0)
            std::cout << i << std::endl;
    }
}
