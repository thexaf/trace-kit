trace-kit
=========

Set of tools to generate traces that are interesting to analyze.

usage
-----

Build the test programs:

```
cmake .
make
```

Record traces:
```
recipes/[the name of a recipe]
```

LTTng installation
------------------
This repository provides a script to install LTTng with addons.
```
lttng/install
```