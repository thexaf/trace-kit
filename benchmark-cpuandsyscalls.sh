#!/bin/bash

control-addons.sh load
sudo lttng list -k > /dev/null
sudo rmmod lttngprofile
recipes/helpers/killapps.sh
lttng destroy 2> /dev/null

# Nothing
echo 
echo 
echo 
echo 
echo 
echo 'Doing nothing.'
./tk-cpuandsyscalls 10 > benchmarks/microbenchmark_nothing.csv

# CPU only.
echo 
echo 
echo 
echo 
echo 
echo 'Doing CPU only.'

pushd ~/src/lttng-profile
git checkout benchmark_cpuonly
./compile.sh
popd
sleep 2
LD_PRELOAD=/usr/local/lib/liblttng-profile.so ./tk-cpuandsyscalls 10 > benchmarks/microbenchmark_cpuonly.csv


# Syscalls, pas de signal.
echo 
echo 
echo 
echo 
echo 
echo 'Doing syscalls, no signal.'

pushd ~/src/lttng-profile
git checkout benchmark_syscallonly
./compile.sh
popd

pushd ~/src/lttng-profile-modules
git checkout nosignal
./install-and-start.sh
popd

sleep 2
LD_PRELOAD=/usr/local/lib/liblttng-profile.so ./tk-cpuandsyscalls 10 > benchmarks/microbenchmark_syscallsonly_nosignal.csv

# Syscalls, avec signal.
echo 
echo 
echo 
echo 
echo 
echo 'Doing syscalls, with signal.'

pushd ~/src/lttng-profile-modules
git checkout master
./install-and-start.sh
popd

sleep 2
LD_PRELOAD=/usr/local/lib/liblttng-profile.so ./tk-cpuandsyscalls 10 > benchmarks/microbenchmark_syscallsonly_withsignal.csv

# CPU et syscalls, pas LTTng
echo 
echo 
echo 
echo 
echo 
echo 'Doing everything, no LTTng.'

pushd ~/src/lttng-profile
git checkout master
./compile.sh
popd

pushd ~/src/lttng-profile-modules
git checkout master
./install-and-start.sh
popd

sleep 2
LD_PRELOAD=/usr/local/lib/liblttng-profile.so ./tk-cpuandsyscalls 10 > benchmarks/microbenchmark_everything_nolttng.csv

# CPU et syscalls, avec LTTng
echo 
echo 
echo 
echo 
echo 
echo 'Doing everything, with LTTng.'

lttng create trace
recipes/helpers/events.sh
lttng start

sleep 2
LD_PRELOAD=/usr/local/lib/liblttng-profile.so ./tk-cpuandsyscalls 10 > benchmarks/microbenchmark_everything_withlttng.csv

lttng destroy
